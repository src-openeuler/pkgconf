%global pkgconf_libdirs %{_libdir}/pkgconfig:%{_datadir}/pkgconfig

Name:           pkgconf
Version:        2.3.0
Release:        1
Summary:        Package compiler and linker metadata toolkit

License:        ISC
URL:            http://pkgconf.org/
Source0:        https://distfiles.dereferenced.org/%{name}/%{name}-%{version}.tar.xz

BuildRequires:  make gcc
#tests
BuildRequires:  kyua, atf-tests

Provides:       pkgconfig(pkgconf) = %{version}
Conflicts:      pkgconfig < 1:%{version}
Obsoletes:      pkgconfig < 1:%{version}
Provides:       pkgconfig = 1:%{version}
Provides:       pkgconfig%{?_isa} = 1:%{version}
Provides:       pkgconfig(pkg-config) = %{version}
Provides:       pkg-config = %{version}
Provides:       pkgconf-pkg-config = %{version}
Obsoletes:      pkgconf-pkg-config < %{version}
Provides:       pkgconf-m4 = %{version}
Obsoletes:      pkgconf-m4 < %{version}
Provides:       libpkgconf = %{version}
Obsoletes:      libpkgconf < %{version}

%description
pkgconf is a program which helps to configure compiler and linker flags for development frameworks. 
It is similar to pkg-config from freedesktop.org, providing additional functionality while also
maintaining compatibility.

%package        devel
Summary:        Development files for lib%{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Provides:       libpkgconf-devel = %{version}
Obsoletes:      libpkgconf-devel < %{version}

%description    devel
Development headers and auxiliary files for developing apps for %{name}.

%package_help

%package        -n bomtool
Summary:        Simple SBOM generator using pc(5) files
Requires:       %{name}%{?_isa} = %{version}-%{release}
Obsoletes:      %{name}-bomtool < %{version}-%{release}

%description    -n bomtool
bomtool is a program which helps generate a Software Bill of Materials
manifest using data from installed .pc files.

%prep
%autosetup -p1

%build
%configure --with-pkg-config-dir=%{pkgconf_libdirs} \
           --with-system-includedir=%{_includedir} \
           --with-system-libdir=%{_libdir}

%make_build


%check
%make_build check

%install
%make_install

%delete_la_and_a

mkdir -p %{buildroot}%{_sysconfdir}/pkgconfig/personality.d
mkdir -p %{buildroot}%{_datadir}/pkgconfig/personality.d

# pkgconf rpm macros
mkdir -p %{buildroot}%{_rpmmacrodir}/

cat > %{buildroot}%{_rpmmacrodir}/macros.pkgconf <<EOM
%%pkgconfig_personalitydir %{_datadir}/pkgconfig/personality.d
EOM

cat <<EOF > %{buildroot}%{_bindir}/%{_target_platform}-pkg-config
#!/bin/sh

# Platform-specific version of pkg-config
# Platform----%{_target_platform}

export PKG_CONFIG_LIBDIR="\${PKG_CONFIG_LIBDIR:-%{pkgconf_libdirs}}"
export PKG_CONFIG_SYSTEM_INCLUDE_PATH="\${PKG_CONFIG_SYSTEM_INCLUDE_PATH:-%{_includedir}}"
export PKG_CONFIG_SYSTEM_LIBRARY_PATH="\${PKG_CONFIG_SYSTEM_LIBRARY_PATH:-%{_libdir}}"

exec pkgconf "\$@"
EOF

ln -sf pkgconf %{buildroot}%{_bindir}/pkg-config
echo ".so man1/pkgconf.1" > %{buildroot}%{_mandir}/man1/pkg-config.1

mkdir -p %{buildroot}%{_libdir}/pkgconfig
mkdir -p %{buildroot}%{_datadir}/pkgconfig

%files
%license COPYING
%doc README.md AUTHORS NEWS
%{_bindir}/%{name}
%{_rpmmacrodir}/macros.pkgconf
%{_libdir}/lib%{name}*.so.*
%{_datadir}/aclocal/pkg.m4
%{_bindir}/pkg-config
%attr(755,root,root) %{_bindir}/%{_target_platform}-pkg-config
%dir %{_libdir}/pkgconfig
%dir %{_datadir}/pkgconfig
%dir %{_sysconfdir}/pkgconfig
%dir %{_sysconfdir}/pkgconfig/personality.d
%dir %{_datadir}/pkgconfig/personality.d

%files devel
%{_libdir}/lib%{name}*.so
%{_includedir}/%{name}/
%{_libdir}/pkgconfig/lib%{name}.pc

%files help
%{_mandir}/*/*

%files -n bomtool
%{_bindir}/bomtool

%changelog
* Fri Dec 06 2024 Funda Wang <fundawang@yeah.net> - 2.3.0-1
- update to 2.3.0
- rename bomtool package
- Add personality.d directories for cross-targets
- Add pkgconf rpm macros for pkgconf directories

* Tue Jul  9 2024 warlcok <hunan@kylinos.cn> - 2.1.1-1
- upgrade version to 2.1.1

* Mon Jul 24 2023 dongyuzhen <dongyuzhen@h-partners.com> - 1.9.5-1
- upgrade version to 1.9.5

* Sun Jan 29 2023 dongyuzhen <dongyuzhen@h-partners.com> - 1.8.0-3
- fix CVE-2023-24056

* Thu May 05 2022 shixuantong <shixuantong@h-partners.com> - 1.8.0-2
- Type: NA
- ID: NA
- SUG: NA
- DESC:fix changelog error

* Sat Dec 25 2021 tianwei <tianwei12@huawei.com> - 1.8.0-1
- Type: NA
- ID: NA
- SUG: NA
- DESC:upgrade version to 1.8.0

* Thu Jul 16 2020 shixuantong <shixuantong@huawei.com> - 1.7.3-1
- Type: NA
- ID: NA
- SUG: NA
- DESC:update to 1.7.3-1 

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.6.3-6
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: strengthen spec

* Thu Nov 28 2019 shenyangyang <shenyangyang4@huawei.com> - 1.6.3-5
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add provides of pkgconfig with epoch 1

* Tue Oct 29 2019 shenyangyang <shenyangyang4@huawei.com> - 1.6.3-4
- Type: enhancement
- ID: NA
- SUG: NA
- DESC:add provides of libpkgconf

* Sat Oct 14 2019 shenyangyang <shenyangyang4@huawei.com> - 1.6.3-3
- Type: enhancement
- ID: NA
- SUG: NA
- DESC:revise provides

* Wed Sep 11 2019 hexiaowen <hexiaowen@huawei.com> - 1.6.3-2
- Rebuild

* Tue Jul 31 2018 hexiaowen <hexiaowen@huawei.com> - 1.6.3-1
- Package init
